package gwt_ui.client.forms;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.view.client.MultiSelectionModel;
import com.google.gwt.view.client.SingleSelectionModel;

import gwt_sql.client.DBService;
import gwt_sql.client.DBServiceAsync;
import gwt_sql.shared.DBException;
import gwt_sql.shared.DBRecord;
import gwt_sql.shared.DBTable;
import gwt_sql.shared.DebugUtils;
import gwt_ui.client.controls.Control;
import gwt_ui.client.controls.dbCheckBox;
import gwt_ui.client.controls.dbDateBox;
import gwt_ui.client.controls.dbDateTimeBox;
import gwt_ui.client.controls.dbListBox;
import gwt_ui.client.controls.dbPickBox;
import gwt_ui.client.controls.dbRadioButton;
import gwt_ui.client.controls.dbTextArea;
import gwt_ui.client.controls.dbTextBox;

/**
 * Super class for the forms in the application
 * 
 * @author Fagadar-Ghisa Bogdan
 *
 */

public class VForm extends Composite implements IForm {

	/**
	 * DBRecord that will be used by this and form childs
	 */
	public DBRecord R, R_BUFFER;

	/**
	 * Dialog box reference
	 */
	public ClosableDialogBox dialogBoxForm;

	/**
	 * LoadingForm instance, used by the forms to show the loading overlay
	 */
	protected LoadingForm loadingFrm = new LoadingForm();

	/**
	 * List of all controls in the form - added with AddControl and used for
	 * Refresh
	 */
	public List<Control> formControls = new ArrayList<Control>();

	/**
	 * Pointer to the caller form - will handle the onReturn event
	 */
	protected VForm callerForm = null;
	/**
	 * String that will be used for onReturn type param.
	 * 
	 * @see Select()
	 */
	protected String callerVarName;

	/**
	 * The selection model that will be used when declaring a CellTable that
	 * needs handlings on row select.
	 */
	protected SingleSelectionModel<DBRecord> selectionModel;

	/**
	 * The selection model that will be used when declaring a CellTable that
	 * needs handlings on multiple rows selection.
	 */
	protected MultiSelectionModel<DBRecord> multiSelectionModel;
	/**
	 * A list of DBRecord that will be used to populate the CellTable. Usually
	 * this will setup using a dataProvider.getList() call;
	 */
	protected List<DBRecord> list = null;
	/**
	 * Will store the selected row DBRecord from a CellTable
	 */
	protected DBRecord selected;

	/**
	 * Will store the multi selected DBRecords from a CellTable
	 */
	protected DBTable multiSelected = new DBTable();

	/**
	 * Will be used for autoincrement fields
	 */
	public String lastInsertedId;

	/**
	 * List fost mandatory fields, added with addMandatory verified with
	 * verifyMandatory
	 */
	private List<String> mandatoryFields = null;
	/**
	 * List of highlight fields
	 */
	private List<String> highlightFields = null;

	/**
	 * List of disabled fields
	 */
	private List<String> disabledFields = null;

	/**
	 * List of hidden fields
	 */
	private List<String> hiddenFields = null;

	/**
	 * Database communication service
	 */
	protected final DBServiceAsync dbService = GWT.create(DBService.class);

	/**
	 * Add a control to the list of controls (will be used in refreshControls)
	 * 
	 * @param c
	 *           Control object to be added
	 */
	protected void addControl(Control c) {
		formControls.add(c);
	}

	/**
	 * Set the R for all the controls previously added with AddControl
	 * 
	 * @param R
	 *           DBRecord used for linking Controls to database table
	 */
	protected void setMyControls(DBRecord R) {
		for (int i = 0; i < formControls.size(); i++) {
			Control c = formControls.get(i);
			c.setR(R);
		}
	}

	/**
	 * Set the R for all db Controls and refresh them after
	 * 
	 * @param R
	 */
	protected void refreshMyControls(DBRecord R) {
		setMyControls(R);
		refreshMyControls();
	}

	/**
	 * Refresh all controls (previously added with AddControl)
	 */
	protected void refreshMyControls() {
		for (int i = 0; i < formControls.size(); i++) {
			try {
				Control C = formControls.get(i);
				C.refresh();
			} catch (Exception e) {
				DebugUtils
						.W("RefreshMyControls.Exception on control " + (i + 1) + " field " + formControls.get(i).getLinkedField() + " \n maybe is null ?");
			}
		}
	}

	/**
	 * Method needed for the return of a async call.
	 */
	@Override
	public void onReturn(String type, DBRecord R) {
	}

	@Override
	public void onReturnTable(String type, DBTable T) {
	}

	/**
	 * Will save to database the DBRecord
	 * 
	 * @param R
	 *           DBRecord to be saved
	 */
	public void DesktopForm_saveDBRecord(final DBRecord R) {
		/* save without key */
		DesktopForm_saveDBRecord(R, "");
	}

	/**
	 * Will save the DBRecord to database and will return the result to onReturn
	 * 
	 * @param R
	 *           DBRecord to be saved
	 * @param key
	 *           return via onReturn with DesktopForm_saveDBRecord + key
	 */
	public void DesktopForm_saveDBRecord(final DBRecord R, final String key) {
		/* save */
		dbService.saveDBRecord(R, new AsyncCallback<String>() {
			@Override
			public void onSuccess(String result) {
				lastInsertedId = result;
				onReturn("DesktopForm_saveDBRecord" + key, R);
			}

			@Override
			public void onFailure(Throwable caught) {
				String details = caught.getMessage();
				if (caught instanceof DBException)
					DebugUtils.W("Fail !" + ((DBException) caught).getMessage());
				else
					DebugUtils.W("DesktopForm_saveDBRecord fail - " + details);
			}
		});
	}

	/**
	 * Delete record from database
	 * 
	 * @param R
	 *           The DBRecord that will be deleted
	 * @return return via onReturn with DesktopForm_deleteDBRecord
	 */
	public void DesktopForm_deleteDBRecord(final DBRecord R) {
		/* delete */
		DesktopForm_deleteDBRecord(R.tableName, R.KeyName, R.KeyValue, "");
	}

	/**
	 * Delete from database with condition column=value
	 * 
	 * @param tableName
	 *           The name of the database table
	 * @param colName
	 *           The name of the column used in where condition
	 * @param colValue
	 *           The value of the column used in where condition
	 * 
	 * @return return via onReturn with DesktopForm_deleteDBRecord
	 */
	public void DesktopForm_deleteDBRecord(String tableName, String colName, String colValue) {
		DesktopForm_deleteDBRecord(tableName, colName, colValue, "");
	}

	/**
	 * Delete from database with condition column=value
	 * 
	 * @param tableName
	 *           The name of the database table
	 * @param colName
	 *           The name of the column used in where condition
	 * @param colValue
	 *           The value of the column used in where condition
	 * @param type
	 *           Type will be used on the onReturn method
	 * 
	 * @return return via onReturn with DesktopForm_deleteDBRecord + type
	 */
	public void DesktopForm_deleteDBRecord(String tableName, String colName, String colValue, final String type) {
		dbService.deleteDBRecord(tableName, colName, colValue, new AsyncCallback<String>() {
			@Override
			public void onSuccess(String result) {
				DBRecord RNULL = null;
				onReturn("DesktopForm_deleteDBRecord" + type, RNULL);
			}

			@Override
			public void onFailure(Throwable caught) {
				if (caught instanceof DBException)
					DebugUtils.W(((DBException) caught).getMessage());
				else
					DebugUtils.W("DesktopForm_deleteDBRecord Exception");
			}
		});
	}

	/**
	 * Create an empty DBRecord
	 * 
	 * @param tableName
	 *           Name of the table
	 * @param colName
	 *           Seek column - optional
	 * @param colValue
	 *           Seek value - optional
	 * @param colKeyName
	 *           The id column name of the table
	 * 
	 * @return return via onReturn with DesktopForm_GetBlankDBRecord
	 */
	public void DesktopForm_GetBlankDBRecord(String tableName, String colName, String colValue, String colKeyName) {
		DesktopForm_GetBlankDBRecord(tableName, colName, colValue, colKeyName, "");
	}

	/**
	 * Create an empty DBRecord
	 * 
	 * @param tableName
	 *           Name of the table
	 * @param colName
	 *           Seek column - optional
	 * @param colValue
	 *           Seek value - optional
	 * @param colKeyName
	 *           The id column name of the table
	 * @param type
	 *           The string that will be added to method name on the onReturn
	 *           method
	 * 
	 * @return return via onReturn with DesktopForm_GetBlankDBRecord + type
	 */
	public void DesktopForm_GetBlankDBRecord(String tableName, String colName, String colValue, String colKeyName, final String type) {

		dbService.GetBlankDBRecord(tableName, colName, colValue, colKeyName, new AsyncCallback<DBRecord>() {

			@Override
			public void onSuccess(DBRecord result) {
				try {

					/*
					 * Refresh only if it's a call from the main form (type is empty)
					 */
					if (type.isEmpty()) {
						R = result;
						refreshMyControls(R);
					}
					onReturn("DesktopForm_GetBlankDBRecord" + type, result);

				} catch (Exception e) {
					DebugUtils.W(e.toString());
					DebugUtils.W("DesktopForm_GetBlankDBRecord Exception");
				}
			}

			@Override
			public void onFailure(Throwable caught) {
				String details = caught.getMessage();
				if (caught instanceof DBException)
					DebugUtils.W(((DBException) caught).getMessage());
				else
					DebugUtils.W("DesktopForm_GetBlankDBRecord fail - " + details);
			}
		});

	}

	/**
	 * Get one record from the database by column=value and fill the DBRecord
	 * parameter with the result
	 * 
	 * @param tableName
	 *           The name of the table to search into
	 * @param colName
	 *           Column name used in where clause
	 * @param colValue
	 *           Column value used in where clause
	 * 
	 * @return return via onReturn with DesktopForm_GetDBRecord
	 */
	public void DesktopForm_GetDBRecord(String tableName, String colName, String colValue) {
		DesktopForm_GetDBRecord(tableName, colName, colValue, null, "");
	}

	/**
	 * Get one record from the database by column=value and fill the DBRecord
	 * parameter with the result
	 * 
	 * @param tableName
	 *           The name of the table to search into
	 * @param colName
	 *           Column name used in where clause
	 * @param colValue
	 *           Column value used in where clause
	 * @param type
	 *           The string added to the method name used in onReturn method
	 * 
	 * @return return via onReturn with DesktopForm_GetDBRecord + type
	 */
	public void DesktopForm_GetDBRecord(String tableName, String colName, String colValue, final DBRecord R_RETURN, final String type) {

		dbService.GetDBRecord(tableName, colName, colValue, new AsyncCallback<DBRecord>() {

			@Override
			public void onSuccess(DBRecord result) {
				try {
					/*
					 * Refresh only if it's a call from the main form (type is empty)
					 */
					if (type.isEmpty()) {
						R = result;
						refreshMyControls(R);
					}

					onReturn("DesktopForm_GetDBRecord" + type, result);

				} catch (Exception e) {
					DebugUtils.W(e.toString());
					DebugUtils.W("DesktopForm_GetDBRecord Exception");
				}
			}

			@Override
			public void onFailure(Throwable caught) {
				String details = caught.getMessage();
				if (caught instanceof DBException)
					DebugUtils.W(((DBException) caught).getMessage());
				else
					DebugUtils.W("DesktopForm_GetDBRecord fail - " + details);
			}
		});

	}

	/**
	 * 
	 * Get a record from the database using a specified SQL command
	 * 
	 * @param strSQLCommand
	 *           The sql command to be executed
	 * @param type
	 *           The string used on the onReturn method
	 * 
	 * @return return via onReturn with DesktopForm_GetDBRecordForCondition +
	 *         type
	 */
	public void DesktopForm_GetDBRecordForCondition(String strSQLCommand, final String type) {

		dbService.GetDBRecordForConditon(strSQLCommand, new AsyncCallback<DBRecord>() {

			@Override
			public void onSuccess(DBRecord result) {
				try {
					onReturn("DesktopForm_GetDBRecordForCondition" + type, result);
				} catch (Exception e) {
					DebugUtils.W(e.toString());
					DebugUtils.W("DesktopForm_GetDBRecordForCondition Exception");
				}
			}

			@Override
			public void onFailure(Throwable caught) {
				String details = caught.getMessage();
				if (caught instanceof DBException)
					DebugUtils.W(((DBException) caught).getMessage());
				else
					DebugUtils.W("DesktopForm_GetDBRecord fail - " + details);
			}
		});

	}

	/**
	 * Execute an sql script without a specified return result (using
	 * executeUpdate) if the sql return a result, an exception will be fired
	 * 
	 * @param strSQLCommand
	 *           The sql command to be executed
	 * @param type
	 *           The string used on onReturn method
	 * 
	 * @return return via onReturn with DesktopForm_executeResultSetNoOutput +
	 *         key
	 */
	public void DesktopForm_executeResultSetNoOutput(String strSQLCommand, final String type) {
		dbService.executeResultSetNoOutput(strSQLCommand, new AsyncCallback<String>() {
			@Override
			public void onSuccess(String result) {
				onReturn("DesktopForm_executeResultSetNoOutput" + type, R);
			}

			@Override
			public void onFailure(Throwable caught) {
				String details = caught.getMessage();
				if (caught instanceof DBException)
					DebugUtils.W(((DBException) caught).getMessage());
				else
					DebugUtils.W("DesktopForm_executeResultSetNoOutput fail - " + details);
			}
		});
	}

	/**
	 * Execute an sql script without a specified return result (using
	 * executeQuery) if the sql return a result, an exception will be fired
	 * 
	 * @param strSQLCommand
	 * 
	 * @param type
	 *           The string used on onReturn method
	 * 
	 * @return return via onReturn with DesktopForm_executeNoResultSet + type
	 */

	public void DesktopForm_executeNoResultSet(String strSQLCommand, final String type) {
		/* save */
		dbService.executeNoResultSet(strSQLCommand, new AsyncCallback<String>() {
			@Override
			public void onSuccess(String result) {
				onReturn("DesktopForm_executeNoResultSet" + type, R);
			}

			@Override
			public void onFailure(Throwable caught) {
				String details = caught.getMessage();
				if (caught instanceof DBException)
					DebugUtils.W(((DBException) caught).getMessage());
				else
					DebugUtils.W("DesktopForm_executeNoResultSet fail - " + details);
			}
		});
	}

	/**
	 * Save an entire table
	 * 
	 * @param dbTable
	 *           DBTable to be saved
	 * @return return via onReturn with DesktopForm_saveDBTable
	 */
	public void DesktopForm_saveDBTable(final DBTable dbTable) {
		DesktopForm_saveDBTable(dbTable, "");
	}

	/**
	 * Save a table with parametrised onReturn
	 * 
	 * @param dbTable
	 *           Table to save
	 * @param type
	 *           Type for onReturn text
	 * 
	 * @return return via onReturn with DesktopForm_saveDBTable + key
	 */
	public void DesktopForm_saveDBTable(final DBTable dbTable, final String type) {
		/* save */
		dbService.saveDBTable(dbTable, new AsyncCallback<DBTable>() {
			public void onSuccess(DBTable result) {
				DBRecord RNULL = null;
				onReturn("DesktopForm_saveDBTable" + type, RNULL);
			}

			@Override
			public void onFailure(Throwable caught) {
				String details = caught.getMessage();
				if (caught instanceof DBException)
					DebugUtils.W("Fail !" + ((DBException) caught).getMessage());
				else
					DebugUtils.W("DesktopForm_saveDBTable fail - " + details);
			}

		});
	}

	/**
	 * Get one table from the database by column=value and fill the DBTable
	 * parameter with the result
	 * 
	 * @param tableName
	 *           The name of the table to search into
	 * @param p_strKeyName
	 *           KeyName of the table
	 * @param p_strFilterCondition
	 *           The filter condition
	 * @param type
	 *           The string added to the method name used in onReturn method
	 * 
	 * @return return via onReturn with DesktopForm_GetDBTable + type
	 */
	public void DesktopForm_GetDBTable(String p_strTableName, String p_strKeyName, String p_strFilterCondition, final String type) {

		dbService.getDBTable(p_strTableName, p_strKeyName, p_strFilterCondition, new AsyncCallback<DBTable>() {

			@Override
			public void onSuccess(DBTable result) {
				try {

					onReturnTable("DesktopForm_GetDBTable" + type, result);

				} catch (Exception e) {
					DebugUtils.W(e.toString());
					DebugUtils.W("DesktopForm_GetDBTable Exception");
				}
			}

			@Override
			public void onFailure(Throwable caught) {
				String details = caught.getMessage();
				if (caught instanceof DBException)
					DebugUtils.W(((DBException) caught).getMessage());
				else
					DebugUtils.W("DesktopForm_GetDBTable fail - " + details);
			}
		});

	}

	/**
	 * Get one table from the database by column=value and fill the DBTable
	 * parameter with the result
	 * 
	 * @param tableName
	 *           The name of the table to search into
	 * @param p_strKeyName
	 *           KeyName of the table
	 * @param p_strFilterCondition
	 *           The filter condition
	 * @param type
	 *           The string added to the method name used in onReturn method
	 * 
	 * @return return via onReturn with DesktopForm_GetDBTable + type
	 */
	public void DesktopForm_GetDBTable(String p_strTableName, String p_strKeyName, String p_strFilterCondition, String p_strOrderCondition,
			final String type) {

		dbService.getDBTable(p_strTableName, p_strKeyName, p_strFilterCondition, p_strOrderCondition, new AsyncCallback<DBTable>() {

			@Override
			public void onSuccess(DBTable result) {
				try {

					onReturnTable("DesktopForm_GetDBTable" + type, result);

				} catch (Exception e) {
					DebugUtils.W(e.toString());
					DebugUtils.W("DesktopForm_GetDBTable Exception");
				}
			}

			@Override
			public void onFailure(Throwable caught) {
				String details = caught.getMessage();
				if (caught instanceof DBException)
					DebugUtils.W(((DBException) caught).getMessage());
				else
					DebugUtils.W("DesktopForm_GetDBTable fail - " + details);
			}
		});

	}

	/**
	 * Get one table from the database by column=value and fill the DBTable
	 * parameter with the result
	 * 
	 * @param tableName
	 *           The name of the table to search into
	 * @param p_strKeyName
	 *           KeyName of the table
	 * @param p_strFilterCondition
	 *           The filter condition
	 * @param type
	 *           The string added to the method name used in onReturn method
	 * 
	 * @return return via onReturn with DesktopForm_GetDBTable + type
	 */
	public void DesktopForm_GetDBTable(String strSQLCommand, final String type) {

		dbService.getDBTable(strSQLCommand, new AsyncCallback<DBTable>() {

			@Override
			public void onSuccess(DBTable result) {
				try {

					onReturnTable("DesktopForm_GetDBTable" + type, result);

				} catch (Exception e) {
					DebugUtils.W(e.toString());
					DebugUtils.W("DesktopForm_GetDBTable Exception");
				}
			}

			@Override
			public void onFailure(Throwable caught) {
				String details = caught.getMessage();
				if (caught instanceof DBException)
					DebugUtils.W(((DBException) caught).getMessage());
				else
					DebugUtils.W("DesktopForm_GetDBTable fail - " + details);
			}
		});

	}

	/**
	 * Action for the Select button from DialogSelectForm
	 */
	public void Select() {
		selected = selectionModel.getSelectedObject();

		if (selected != null) {
			if (callerForm != null) {
				callerForm.onReturn(callerVarName, selected);
			}

			if (dialogBoxForm != null) {
				dialogBoxForm.hide();
			}
		}
	}

	/**
	 * Hide the dialog box
	 */
	public void hide() {
		if (dialogBoxForm != null)
			dialogBoxForm.hide();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see gwtSql.client.forms.IForm#returnSelected()
	 */
	@Override
	public DBRecord returnSelected() {
		return selected;
	}

	/**
	 * Sets the caller form for a dialog box
	 * 
	 * @param f
	 */
	public void setCallerForm(VForm f) {
		callerForm = f;
	}

	/**
	 * Apply rights for the current class
	 * 
	 * @param strRights
	 *           = TheApp.loginInfo.User.getString("RIGHTS")
	 * @param className
	 *           = this.getClass().getName() example of call :
	 *           ApplyRights(TheApp.loginInfo.User.getString("RIGHTS"),
	 *           this.getClass().getName());
	 */
	public void applyRights(String strRights, String className) {

		strRights = strRights.replaceAll("<div>", "\n");
		strRights = strRights.replaceAll("</div>", "\n");
		strRights = strRights.replaceAll("<br>", "\n");
		strRights = strRights.replaceAll(" ", "");
		strRights = strRights.replaceAll("\n\n", "\n");

		String[] aRights = strRights.split("\n");
		boolean lShow = false;

		// cut at the $ sign
		int nPos = className.indexOf("$");
		if (nPos > 0)
			className = className.substring(0, nPos);
		className = className.trim() + ".";
		//
		String right;
		for (int i = 0; i < aRights.length; i++) {
			// search the class name in the string
			right = aRights[i];
			// if + is show if - is hide
			if (right.startsWith("-")) {
				lShow = false;
				right = right.replaceAll("-", "");
			} else {
				lShow = true;
			}

			//
			if (right.contains(className)) {
				right = right.replaceAll(className, "");
				if (lShow)
					showControl(right);
				else
					hideControl(right);
			}
		}
	}

	/**
	 * Hide controls ... with the specified id ...and more 5 ... with the id_1,
	 * id_2, ... id_5
	 * 
	 * @param id
	 *           The id of the element to be hidden
	 */
	public native void hideControl(String id)
	/*-{
		if ($doc.getElementById(id) != null)
			$doc.getElementById(id).style.display = "none";

		for (i = 1; i < 5; i++) {
			id1 = id + "_" + i;

			if ($doc.getElementById(id1) != null)
				$doc.getElementById(id1).style.display = "none";
		}
	}-*/;

	/**
	 * Show controls ...with the specified id ...and more 5 ... with the id_1,
	 * id_2, ... id_5
	 * 
	 * @param id
	 *           The id of the element to be shown
	 */
	public native void showControl(String id)
	/*-{
		if ($doc.getElementById(id) != null)
			$doc.getElementById(id).style.display = "block";

		for (i = 1; i < 5; i++) {
			id1 = id + "_" + i;

			if ($doc.getElementById(id1) != null)
				$doc.getElementById(id1).style.display = "block";
		}
	}-*/;

	/**
	 * Will display the given message in the messages div.
	 * 
	 * @param msg
	 *           The string that we wish to display
	 * @param msgClass
	 *           The class that will be applied in the message div. "sad" for
	 *           error messages, "happy" for successful messages
	 */
	public native void showMessage(String msg, String msgClass)
	/*-{
		msgDiv = $doc.getElementById("msgText");
		if (msgDiv != null) {
			msgDiv.innerHTML = "<li class=\"" + msgClass + "\">" + msg
					+ "</li>";
			setTimeout(function() {
				$doc.getElementById("msgText").innerHTML = "";
			}, 5000);
		}
	}-*/;

	/**
	 * Will hide the message displayed in the message div.
	 */
	public native void hideMessage()
	/*-{
		if ($doc.getElementById("msgText") != null) {
			$doc.getElementById("msgText").innerHTML = "";
		}
	}-*/;

	/**
	 * Add the mandatory fields of the mandatory list.
	 * 
	 * @param fields
	 *           The list of mandatory fields from
	 *           TheApp._VAR(Frm.this.getClass().getSimpleName() + _mandatory);
	 */
	public void addMandatory(String fields) {
		if (mandatoryFields == null)
			mandatoryFields = new ArrayList<String>();
		String[] arrMandatory = fields.split(",");
		for (String fldName : arrMandatory) {
			mandatoryFields.add(fldName.trim());
		}
	}

	/**
	 * Test for each field in the mandatory list and return false if empty or
	 * null is found
	 * 
	 * @return boolean
	 */
	public boolean verifMandatory() {
		String errorMsg = verifMandWithMessage();

		if (errorMsg.isEmpty()) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Test for each field in the mandatory list and return false if empty or
	 * null is found
	 * 
	 * @return The error message
	 */
	public String verifMandWithMessage() {
		String errorMsg = "";
		if (mandatoryFields != null) {
			for (Control ctrl : formControls) {
				// TODO Refactor switch statement
				switch (ctrl.getType()) {
				case "dbTextBox":
					dbTextBox txtBox = (dbTextBox) ctrl;
					// check for at least one ASCII alphanumeric character
					if (R.get(txtBox.getLinkedField()) != null && mandatoryFields.contains(txtBox.getLinkedField())
							&& !R.getString(txtBox.getLinkedField()).matches(".*\\w.*")) {
						errorMsg += txtBox.getLinkedField() + " este obligatoriu <br />";
						txtBox.addStyleName("mandatory-field");
					} else {
						txtBox.removeStyleName("mandatory-field");
					}
					break;
				case "dbTextArea":
					dbTextArea txtArea = (dbTextArea) ctrl;
					// check for at least one ASCII alphanumeric character
					if (R.get(txtArea.getLinkedField()) != null && mandatoryFields.contains(txtArea.getLinkedField())
							&& !R.getString(txtArea.getLinkedField()).matches(".*\\w.*")) {
						errorMsg += txtArea.getLinkedField() + " este obligatoriu <br />";
						txtArea.addStyleName("mandatory-field");
					} else {
						txtArea.removeStyleName("mandatory-field");
					}
					break;
				case "dbCheckBox":
					dbCheckBox ckBox = (dbCheckBox) ctrl;
					if (!ckBox.getValue() && mandatoryFields.contains(ckBox.getLinkedField())) {
						errorMsg += ckBox.getLinkedField() + " este obligatoriu <br />";
						ckBox.addStyleName("mandatory-field");
					} else {
						ckBox.removeStyleName("mandatory-field");
					}
					break;
				case "dbDateBox":
					dbDateBox dateBox = (dbDateBox) ctrl;
					if (R.get(dateBox.getLinkedField()) != null && mandatoryFields.contains(dateBox.getLinkedField())
							&& !R.getString(dateBox.getLinkedField()).matches(".*\\w.*")) {
						errorMsg += dateBox.getLinkedField() + " este obligatoriu <br />";
						dateBox.addStyleName("mandatory-field");
					} else {
						dateBox.removeStyleName("mandatory-field");
					}
					break;
				case "dbDateTimeBox":
					dbDateTimeBox dateTBox = (dbDateTimeBox) ctrl;
					if (R.get(dateTBox.getLinkedField()) != null && mandatoryFields.contains(dateTBox.getLinkedField())
							&& !R.getString(dateTBox.getLinkedField()).matches(".*\\w.*")) {
						errorMsg += dateTBox.getLinkedField() + " este obligatoriu <br />";
						dateTBox.addStyleName("mandatory-field");
					} else {
						dateTBox.removeStyleName("mandatory-field");
					}
					break;
				case "dbListBox":
					dbListBox listBox = (dbListBox) ctrl;
					if (R.get(listBox.getLinkedField()) != null && mandatoryFields.contains(listBox.getLinkedField())
							&& !R.getString(listBox.getLinkedField()).matches(".*\\w.*")) {
						errorMsg += listBox.getLinkedField() + " este obligatoriu <br />";
						listBox.addStyleName("mandatory-field");
					} else {
						listBox.removeStyleName("mandatory-field");
					}
					break;
				case "dbPickBox":
					dbPickBox pkBox = (dbPickBox) ctrl;
					if (R.get(pkBox.getLinkedField()) != null && mandatoryFields.contains(pkBox.getLinkedField())
							&& !R.getString(pkBox.getLinkedField()).matches(".*\\w.*")) {
						errorMsg += pkBox.getLinkedField() + " este obligatoriu <br />";
						pkBox.addStyleName("mandatory-field");
					} else {
						pkBox.removeStyleName("mandatory-field");
					}
					break;
				case "dbRadioButton":
					dbRadioButton rBtn = (dbRadioButton) ctrl;
					if (R.get(rBtn.getLinkedField()) != null && mandatoryFields.contains(rBtn.getLinkedField())
							&& !R.getString(rBtn.getLinkedField()).matches(".*\\w.*")) {
						errorMsg += rBtn.getLinkedField() + " este obligatoriu <br />";
						rBtn.addStyleName("mandatory-field");
					} else {
						rBtn.removeStyleName("mandatory-field");
					}
					break;
				default:
					break;
				}
			}
		}
		return errorMsg;
	}

	/**
	 * Populate highlight fields list
	 * 
	 * @param fields
	 *           A list of fields separated by comma (,)
	 */
	public void addHighlightFields(String fields) {
		if (highlightFields == null) {
			highlightFields = new ArrayList<String>();
		}

		String[] arrHighlight = fields.split(",");
		for (String fldName : arrHighlight) {
			highlightFields.add(fldName.trim());
		}
	}

	/**
	 * Will add a CSS class to the highlight fields
	 */
	public void highlightFields() {
		if (highlightFields != null) {
			for (Control ctrl : formControls) {
				switch (ctrl.getType()) {
				case "dbTextBox":
					dbTextBox txtBox = (dbTextBox) ctrl;
					if (highlightFields.contains(txtBox.getLinkedField())) {
						txtBox.addStyleName("highlight-field");
					}
					break;
				case "dbTextArea":
					dbTextArea txtArea = (dbTextArea) ctrl;
					if (highlightFields.contains(txtArea.getLinkedField())) {
						txtArea.addStyleName("highlight-field");
					}
					break;
				case "dbCheckBox":
					dbCheckBox ckBox = (dbCheckBox) ctrl;
					if (highlightFields.contains(ckBox.getLinkedField())) {
						ckBox.addStyleName("highlight-field");
					}
					break;
				case "dbDateBox":
					dbDateBox dateBox = (dbDateBox) ctrl;
					if (highlightFields.contains(dateBox.getLinkedField())) {
						dateBox.addStyleName("highlight-field");
					}
					break;
				case "dbDateTimeBox":
					dbDateTimeBox dateTBox = (dbDateTimeBox) ctrl;
					if (highlightFields.contains(dateTBox.getLinkedField())) {
						dateTBox.addStyleName("highlight-field");
					}
					break;
				case "dbListBox":
					dbListBox listBox = (dbListBox) ctrl;
					if (highlightFields.contains(listBox.getLinkedField())) {
						listBox.addStyleName("highlight-field");
					}
					break;
				case "dbPickBox":
					dbPickBox pkBox = (dbPickBox) ctrl;
					if (highlightFields.contains(pkBox.getLinkedField())) {
						pkBox.btnGet.addStyleName("highlight-field");
					}
					break;
				case "dbRadioButton":
					dbRadioButton rBtn = (dbRadioButton) ctrl;
					if (highlightFields.contains(rBtn.getLinkedField())) {
						rBtn.addStyleName("highlight-field");
					}
					break;
				default:
					break;
				}
			}
		}
	}

	/**
	 * Populate disabled fields list
	 * 
	 * @param fields
	 *           A list of fields separated by comma (,)
	 */
	public void addDisabledFields(String fields) {
		if (disabledFields == null) {
			disabledFields = new ArrayList<String>();
		}

		String[] arrDisabled = fields.split(",");
		for (String fldName : arrDisabled) {
			disabledFields.add(fldName.trim());
		}
	}

	/**
	 * Will add a CSS class to the disabled fields and will disable the field
	 */
	public void disableFields() {
		if (disabledFields != null) {
			for (Control ctrl : formControls) {
				switch (ctrl.getType()) {
				case "dbTextBox":
					dbTextBox txtBox = (dbTextBox) ctrl;
					if (disabledFields.contains(txtBox.getLinkedField())) {
						txtBox.addStyleName("disabled-field");
						txtBox.setEnabled(false);
					}
					break;
				case "dbTextArea":
					dbTextArea txtArea = (dbTextArea) ctrl;
					if (disabledFields.contains(txtArea.getLinkedField())) {
						txtArea.addStyleName("disabled-field");
						txtArea.setEnabled(false);
					}
					break;
				case "dbCheckBox":
					dbCheckBox ckBox = (dbCheckBox) ctrl;
					if (disabledFields.contains(ckBox.getLinkedField())) {
						ckBox.addStyleName("disabled-field");
						ckBox.setEnabled(false);
					}
					break;
				case "dbDateBox":
					dbDateBox dateBox = (dbDateBox) ctrl;
					if (disabledFields.contains(dateBox.getLinkedField())) {
						dateBox.addStyleName("disabled-field");
						dateBox.setEnabled(false);
					}
					break;
				case "dbDateTimeBox":
					dbDateTimeBox dateTBox = (dbDateTimeBox) ctrl;
					if (disabledFields.contains(dateTBox.getLinkedField())) {
						dateTBox.addStyleName("disabled-field");
						dateTBox.setEnabled(false);
					}
					break;
				case "dbListBox":
					dbListBox listBox = (dbListBox) ctrl;
					if (disabledFields.contains(listBox.getLinkedField())) {
						listBox.addStyleName("disabled-field");
						listBox.setEnabled(false);
					}
					break;
				case "dbPickBox":
					dbPickBox pkBox = (dbPickBox) ctrl;
					if (disabledFields.contains(pkBox.getLinkedField())) {
						pkBox.btnGet.addStyleName("disabled-field");
						pkBox.setEnabled(false);
					}
					break;
				case "dbRadioButton":
					dbRadioButton rBtn = (dbRadioButton) ctrl;
					if (disabledFields.contains(rBtn.getLinkedField())) {
						rBtn.addStyleName("disabled-field");
						rBtn.setEnabled(false);
					}
					break;
				default:
					break;
				}
			}
		}
	}

	/**
	 * Populate hidden fields list
	 * 
	 * @param fields
	 *           A list of fields separated by comma (,)
	 */
	public void addHiddenFields(String fields) {
		if (hiddenFields == null) {
			hiddenFields = new ArrayList<String>();
		}

		String[] arrHidden = fields.split(",");
		for (String fldName : arrHidden) {
			hiddenFields.add(fldName.trim());
		}
	}

	/**
	 * Will add a CSS class to the control to hide the field and will remove the
	 * field from Controls list.
	 */
	public void hideFields() {
		if (hiddenFields != null) {
			ListIterator<Control> iterator = formControls.listIterator();
			while (iterator.hasNext()) {
				Control elem = iterator.next();
				switch (elem.getType()) {
				case "dbTextBox":
					dbTextBox txtBox = (dbTextBox) elem;
					if (hiddenFields.contains(txtBox.getLinkedField())) {
						txtBox.addStyleName("hidden-field");
						txtBox.setEnabled(false);
						iterator.remove();
						txtBox.setValue("");
					}
					break;
				case "dbTextArea":
					dbTextArea txtArea = (dbTextArea) elem;
					if (hiddenFields.contains(txtArea.getLinkedField())) {
						txtArea.addStyleName("hidden-field");
						txtArea.setEnabled(false);
						iterator.remove();
						txtArea.setValue("");
					}
					break;
				case "dbCheckBox":
					dbCheckBox ckBox = (dbCheckBox) elem;
					if (hiddenFields.contains(ckBox.getLinkedField())) {
						ckBox.addStyleName("hidden-field");
						ckBox.setEnabled(false);
						iterator.remove();
						ckBox.setValue(false);
					}
					break;
				case "dbDateBox":
					dbDateBox dateBox = (dbDateBox) elem;
					if (hiddenFields.contains(dateBox.getLinkedField())) {
						dateBox.addStyleName("hidden-field");
						dateBox.setEnabled(false);
						iterator.remove();
						dateBox.setValue(null);
					}
					break;
				case "dbDateTimeBox":
					dbDateTimeBox dateTBox = (dbDateTimeBox) elem;
					if (hiddenFields.contains(dateTBox.getLinkedField())) {
						dateTBox.addStyleName("hidden-field");
						dateTBox.setEnabled(false);
						iterator.remove();
					}
					break;
				case "dbListBox":
					dbListBox listBox = (dbListBox) elem;
					if (hiddenFields.contains(listBox.getLinkedField())) {
						listBox.addStyleName("hidden-field");
						listBox.setEnabled(false);
						iterator.remove();
					}
					break;
				case "dbPickBox":
					dbPickBox pkBox = (dbPickBox) elem;
					if (hiddenFields.contains(pkBox.getLinkedField())) {
						pkBox.btnGet.addStyleName("hidden-field");
						pkBox.setEnabled(false);
						iterator.remove();
					}
					break;
				case "dbRadioButton":
					dbRadioButton rBtn = (dbRadioButton) elem;
					if (hiddenFields.contains(rBtn.getLinkedField())) {
						rBtn.addStyleName("hidden-field");
						rBtn.setEnabled(false);
						iterator.remove();
					}
					break;
				default:
					break;
				}
			}
		}
	}

	/**
	 * Will set if the controls of this form are active
	 * 
	 * @param state
	 */
	public void setControlsState(boolean state) {
		for (Control ctrl : this.formControls) {
			switch (ctrl.getType()) {
			case "dbTextBox":
				dbTextBox txtBox = (dbTextBox) ctrl;
				txtBox.setReadOnly(!state);
				break;
			case "dbTextArea":
				dbTextArea txtArea = (dbTextArea) ctrl;
				txtArea.setReadOnly(!state);
				break;
			case "dbCheckBox":
				dbCheckBox ckBox = (dbCheckBox) ctrl;
				ckBox.setEnabled(state);
				break;
			case "dbDateBox":
				dbDateBox dateBox = (dbDateBox) ctrl;
				dateBox.setEnabled(state);
				break;
			case "dbDateTimeBox":
				dbDateTimeBox dateTBox = (dbDateTimeBox) ctrl;
				dateTBox.setEnabled(state);
				break;
			case "dbListBox":
				dbListBox listBox = (dbListBox) ctrl;
				listBox.setEnabled(state);
				break;
			case "dbPickBox":
				dbPickBox pkBox = (dbPickBox) ctrl;
				pkBox.setEnabled(state);
				break;
			case "dbRadioButton":
				dbRadioButton rBtn = (dbRadioButton) ctrl;
				rBtn.setEnabled(state);
				break;
			default:
				break;
			}
		}
	}

	protected void showLoading() {
		loadingFrm.center();
		loadingFrm.show();
	}

	protected void showLoading(String message) {
		loadingFrm.setMessage(message);
		loadingFrm.center();
		loadingFrm.show();
	}

	protected void hideLoading() {
		loadingFrm.hide();
	}

	/**
	 * Return the callerVarName property
	 * 
	 * @return
	 */
	public String getcallerVarName() {
		return callerVarName;
	}

	@Override
	public DBTable returnMultiSelected() {
		if (multiSelectionModel.getSelectedSet().size() != 0) {
			for (DBRecord rec : multiSelectionModel.getSelectedSet()) {
				multiSelected.add(rec);
			}
		}
		return multiSelected;
	}

}