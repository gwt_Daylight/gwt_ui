package gwt_ui.client.forms;

import java.util.List;

import com.google.gwt.view.client.SingleSelectionModel;

import gwt_sql.shared.DBRecord;

public class VListForm extends BaseDialogBox {

	protected IForm caller_form;
	protected String caller_varName;
	protected SingleSelectionModel<DBRecord> selectionModel;

	protected List<DBRecord> list = null;
	protected DBRecord selected;

	/**
	 * select
	 */
	public void Select() {

		selected = selectionModel.getSelectedObject();
		// DebugUtils.D("try to send ...", 1);
		// DebugUtils.D(selected, 1);
		if (selected != null) {
			// selected
			if (caller_form != null) {
				// DebugUtils.D("send ...", 1);
				// DebugUtils.D(selected, 1);
				caller_form.onReturn(caller_varName, selected);
			}

			hide();
		}
	} // Select()

	/**
	 * call GWT remote
	 * 
	 */

}
