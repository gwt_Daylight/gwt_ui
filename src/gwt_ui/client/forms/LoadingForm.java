package gwt_ui.client.forms;

import com.google.gwt.core.client.GWT;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;

public class LoadingForm extends BaseDialogBox {

	interface MyUiBinder extends UiBinder<Widget, LoadingForm> {
	}

	private static MyUiBinder uiBinder = GWT.create(MyUiBinder.class);

	@UiField(provided = true)
	SimplePanel messageBox;
	@UiField(provided = true)
	HTML msgText;

	/**
	 * Will display a loading image and overlay over the page
	 */
	public LoadingForm() {
		messageBox = new SimplePanel();
		msgText = new HTML();

		addStyleName("round-dialog");
		setWidget(uiBinder.createAndBindUi(this));
	}

	/**
	 * Will set the message that will appear under the loading animation
	 * 
	 * @param message
	 *            The message to be displayed
	 */
	public void setMessage(String message) {
		if (message != null && message.length() > 0) {
			SafeHtmlBuilder builder = new SafeHtmlBuilder();
			builder.appendEscaped(message);
			msgText.setHTML(builder.toSafeHtml());
		}
	}
}