package gwt_ui.client.forms;

import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.LayoutPanel;

import gwt_sql.shared.DBRecord;
import gwt_sql.shared.DBTable;

public class BaseDialogBox extends DialogBox implements IForm {
	public LayoutPanel baseLayoutPanel;
	public ClosableDialogBox VFF;

	/**
	 * constructor
	 */
	public BaseDialogBox() {

		super(false, false);

		// Enable animation.
		setAnimationEnabled(true);

		// Enable glass background.
		setGlassEnabled(true);

		baseLayoutPanel = new LayoutPanel();
		baseLayoutPanel.setStyleName("BasePanel");
		setWidget(baseLayoutPanel);
		baseLayoutPanel.setSize("154px", "48px");

	}

	@Override
	public void onReturn(String type, DBRecord R) {

	}

	@Override
	public void onReturnTable(String type, DBTable T) {

	}

	@Override
	public DBRecord returnSelected() {
		return null;
	}

	@Override
	public DBTable returnMultiSelected() {
		return null;
	}

}
