package gwt_ui.client.forms;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.EventTarget;
import com.google.gwt.dom.client.NativeEvent;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Widget;

import gwt_ui.client.Gwt_ui;

public class ClosableDialogBox extends DialogBox {

	private HorizontalPanel captionPanel = new HorizontalPanel();

	// widget which will be use to close the dialog box
	private Widget closeWidget = null;

	// scroll handler
	HandlerRegistration scrollHandlerReg;

	/**
	 * Window with a closable corner
	 */
	public ClosableDialogBox() {
		super();
		Button b = new Button();
		b.setHTML("<span class=\"bc-button-icon\"> <img alt=\"Inchide fereastra\" src=\"css/icons/bt-cancel-small-square.png\"> </span>");
		b.setStyleName("bc-button bc-button-icon-small");
		closeWidget = b;
		setHTML("<div class=\"dialog-logo\"><img src=\"images/logo-gray.png\" width=\"208\" height=\"14\" alt=\"\"/></div>");

		Window.ScrollHandler _scrollHandler = new Window.ScrollHandler() {
			@Override
			public void onWindowScroll(com.google.gwt.user.client.Window.ScrollEvent event) {

				// if my handler il the last
				if (ClosableDialogBox.this.scrollHandlerReg.equals(Gwt_ui.handlerRegistrationList.get(Gwt_ui.handlerRegistrationList.size() - 1))) {

					if (0 == ClosableDialogBox.this.getOffsetHeight()) {
						return;
					}

					// too down
					// if (Window.getScrollTop() >
					// ClosableDialogBox.this.getAbsoluteTop() +
					// ClosableDialogBox.this.getOffsetHeight()) {
					// GWT.log("too down");
					// GWT.log("Window.getScrollTop() >
					// ClosableDialogBox.this.getAbsoluteTop() +
					// ClosableDialogBox.this.getOffsetHeight()");
					// GWT.log(Window.getScrollTop() + ">" +
					// ClosableDialogBox.this.getAbsoluteTop() + "+" +
					// ClosableDialogBox.this.getOffsetHeight());
					// GWT.log("Window.scrollTo");
					// GWT.log("Window.getScrollLeft(): " + Window.getScrollLeft());
					// GWT.log("ClosableDialogBox.this.getAbsoluteTop(): " +
					// ClosableDialogBox.this.getAbsoluteTop());
					// GWT.log("ClosableDialogBox.this.getOffsetHeight(): " +
					// ClosableDialogBox.this.getOffsetHeight());
					// Window.scrollTo(Window.getScrollLeft(),
					// ClosableDialogBox.this.getAbsoluteTop() +
					// ClosableDialogBox.this.getOffsetHeight() - 10);
					// }

					// if (Window.getScrollTop() > (Window.getScrollTop() +
					// ClosableDialogBox.this.getAbsoluteTop() +
					// ClosableDialogBox.this.getOffsetHeight() + 200)) {
					// if (Window.getScrollTop() >
					// (-ClosableDialogBox.this.getPopupTop() -
					// ClosableDialogBox.this.getScrollTop() +
					// ClosableDialogBox.this.getOffsetHeight())) {
					int x = Window.getScrollTop() + Window.getClientHeight()
							- (ClosableDialogBox.this.getScrollTop() + ClosableDialogBox.this.getAbsoluteTop());
					int y = ClosableDialogBox.this.getOffsetHeight();
					int z = Window.getClientHeight() - (ClosableDialogBox.this.getScrollTop() + ClosableDialogBox.this.getAbsoluteTop());
					if (x > y) {
						// GWT.log("too down");
						// GWT.log("Window.getScrollTop(): " + Window.getScrollTop());
						// GWT.log("Window.getClientHeight(): " +
						// Window.getClientHeight());
						// GWT.log("ClosableDialogBox.this.getAbsoluteTop(): " +
						// ClosableDialogBox.this.getAbsoluteTop());
						// GWT.log("ClosableDialogBox.this.getScrollTop(): " +
						// ClosableDialogBox.this.getScrollTop());
						// GWT.log("ClosableDialogBox.this.getOffsetHeight(): " +
						// ClosableDialogBox.this.getOffsetHeight());
						// GWT.log("" + (Window.getScrollTop() +
						// Window.getClientHeight()
						// - (ClosableDialogBox.this.getScrollTop() +
						// ClosableDialogBox.this.getAbsoluteTop())));
						Window.scrollTo(Window.getScrollLeft(), y - z);
					}

					// Window.scrollTo(Window.getScrollLeft(),
					// ClosableDialogBox.this.getAbsoluteTop() +
					// ClosableDialogBox.this.getOffsetHeight() + 200);
					// Window.scrollTo(Window.getScrollLeft(),
					// -ClosableDialogBox.this.getPopupTop() -
					// ClosableDialogBox.this.getScrollTop() +
					// ClosableDialogBox.this.getOffsetHeight());
					// }

					// too high
					if (Window.getScrollTop() + Window.getClientHeight() < ClosableDialogBox.this.getAbsoluteTop()) {
						GWT.log("too up");
						GWT.log("Window.getScrollTop() + Window.getClientHeight() < ClosableDialogBox.this.getAbsoluteTop()");
						GWT.log(Window.getScrollTop() + "+" + Window.getClientHeight() + "<" + ClosableDialogBox.this.getAbsoluteTop());
						GWT.log("Window.scrollTo");
						GWT.log("Window.getScrollLeft(): " + Window.getScrollLeft());

						GWT.log("ClosableDialogBox.this.getAbsoluteTop(): " + ClosableDialogBox.this.getAbsoluteTop());
						GWT.log("Window.getClientHeight(): " + Window.getClientHeight());
						Window.scrollTo(Window.getScrollLeft(), ClosableDialogBox.this.getAbsoluteTop() - Window.getClientHeight() + 10);
					}

				}
			}
		};

		scrollHandlerReg = com.google.gwt.user.client.Window.addWindowScrollHandler(_scrollHandler);

		// add handler in the list
		Gwt_ui.handlerRegistrationList.add(scrollHandlerReg);

	}

	@Override
	public void setHTML(String html) {
		if (closeWidget != null) {
			setCaption(html, closeWidget);
		} else {
			super.setHTML(html);
		}
	}

	@Override
	public void onUnload() {

		scrollHandlerReg.removeHandler();

		// delete from list
		Gwt_ui.handlerRegistrationList.remove(Gwt_ui.handlerRegistrationList.size() - 1);
	}

	@Override
	public void setHTML(SafeHtml html) {
		if (closeWidget != null) {
			setCaption(html.asString(), closeWidget);
		} else {
			super.setHTML(html);
		}
	}

	/**
	 * Makes a new caption and replace the old one.
	 * 
	 * @param txt
	 * @param w
	 */
	private void setCaption(String txt, Widget w) {
		captionPanel.setWidth("100%");
		captionPanel.add(new HTML(txt));
		captionPanel.add(w);
		captionPanel.setCellHorizontalAlignment(w, HasHorizontalAlignment.ALIGN_RIGHT);
		// make sure that only when you click on this icon the widget will be
		// closed!, don't make the field too width
		captionPanel.setCellWidth(w, "1%");
		captionPanel.addStyleName("Caption");

		// Get the cell element that holds the caption
		Element td = getCellElement(0, 1);

		// Remove the old caption
		td.setInnerHTML("");

		// append our horizontal panel
		td.appendChild(captionPanel.getElement());
	}

	/**
	 * Close handler, which will hide the dialog box
	 */
	private class DialogBoxCloseHandler {
		public void onClick(Event event) {
			hide();
			onClose();
		}
	}

	/**
	 * Function checks if the browser event is was inside the caption region
	 * 
	 * @param event
	 *           browser event
	 * @return true if event inside the caption panel (DialogBox header)
	 */
	protected boolean isHeaderCloseControlEvent(NativeEvent event) {
		// return isWidgetEvent(event, captionPanel.getWidget(1));
		return isWidgetEvent(event, closeWidget);
	}

	/**
	 * Overrides the browser event from the DialogBox
	 */
	@Override
	public void onBrowserEvent(Event event) {
		if (isHeaderCloseControlEvent(event)) {

			switch (event.getTypeInt()) {
			case Event.ONMOUSEUP:
			case Event.ONCLICK:
				new DialogBoxCloseHandler().onClick(event);
				break;
			case Event.ONMOUSEOVER:
				break;
			case Event.ONMOUSEOUT:
				break;
			}

			return;
		}

		// go to the DialogBox browser event
		super.onBrowserEvent(event);
	}

	/**
	 * Function checks if event was inside a given widget
	 * 
	 * @param event
	 *           - current event
	 * @param w
	 *           - widget to prove if event was inside
	 * @return - true if event inside the given widget
	 */
	protected boolean isWidgetEvent(NativeEvent event, Widget w) {
		EventTarget target = event.getEventTarget();

		if (Element.is(target)) {
			boolean t = w.getElement().isOrHasChild(Element.as(target));
			// GWT.log("isWidgetEvent:" + w + ':' + target + ':' + t);
			return t;
		}
		return false;
	}

	/**
	 * called before close
	 */
	public void onClose() {
		// called before close
	}

	// scroll functions
	private native int getPageXOffset()
	/*-{
		if ($wnd.pageXOffset) {
			return $wnd.pageXOffset;
		}
		return -1;
	}-*/;

	private native int getPageYOffset()
	/*-{
		if ($wnd.pageYOffset) {
			return $wnd.pageYOffset;
		}
		return -1;
	}-*/;

	public int getScrollLeft() {
		int left = getPageXOffset();
		if (left == -1) {
			left = Window.getScrollLeft();
		}
		return left;
	}

	public int getScrollTop() {
		int top = getPageYOffset();
		if (top == -1) {
			top = Window.getScrollTop();
		}
		return top;
	}

	public void setCenter() {
		center();
		setPopupPosition(Math.max(getAbsoluteLeft() + 0, 0), Math.max(getScrollTop() + 50, 0));
	}

	// @Override
	// protected void endDragging(MouseUpEvent event) {
	// // Move dialog window behind top border
	// if (this.getAbsoluteTop() < 0) {
	// this.setPopupPosition(this.getPopupLeft(), 0);
	// }
	// // Move dialog window behind bottom border
	// if (this.getAbsoluteTop() > (Window.getClientHeight() -
	// this.getOffsetHeight())) {
	// this.setPopupPosition(this.getPopupLeft(), Window.getClientHeight() -
	// this.getOffsetHeight());
	// }
	//
	// // Move dialog window behind left border
	// if (this.getAbsoluteLeft() < 0) {
	// this.setPopupPosition(0, this.getPopupTop());
	// }
	// // Move dialog window behind right border
	// if (this.getAbsoluteLeft() > (Window.getClientWidth() -
	// this.getOffsetWidth())) {
	// this.setPopupPosition(Window.getClientWidth() - this.getOffsetWidth(),
	// this.getPopupTop());
	// }
	// super.endDragging(event);
	// }

}