package gwt_ui.client.forms;

import gwt_sql.shared.DBRecord;
import gwt_sql.shared.DBTable;

/**
 * Interface for all visual forms
 * 
 * @author Fagadar-Ghisa Bogdan
 *
 */
public interface IForm {

	/**
	 * Is used by the async mechanism to return the result to the caller of the
	 * async call. If you do an async call using VForm async methods
	 * (DesktopForm_ACTION), from a form, the result will be returned on this
	 * method of the caller form.
	 * 
	 * @param type
	 *            The type to check against when the async call returns the
	 *            result (type.equals(DesktopForm_saveDbTable)).
	 * @param R
	 *            The DBRecord returned by the async call.
	 */
	void onReturn(String type, DBRecord R);

	/**
	 * Is used by the async mechanism to return the result to the caller of the
	 * async call. If you do an async call using VForm async methods
	 * (DesktopForm_ACTION), from a form, the result will be returned on this
	 * method of the caller form.
	 * 
	 * @param type
	 *            The type to check against when the async call returns the
	 *            result (type.equals(DesktopForm_saveDbTable)).
	 * @param T
	 *            The DBTable returned by the async call.
	 */
	void onReturnTable(String type, DBTable T);

	/**
	 * Will return the selected record from a popup Form.
	 * 
	 * @return
	 */
	DBRecord returnSelected();

	/**
	 * Will return the multiple selected records from a popup Form.
	 * 
	 * @return
	 */
	DBTable returnMultiSelected();

}
