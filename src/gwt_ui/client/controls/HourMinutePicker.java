package gwt_ui.client.controls;

import java.util.ArrayList;

import com.google.gwt.event.dom.client.BlurEvent;
import com.google.gwt.event.dom.client.BlurHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.FocusEvent;
import com.google.gwt.event.dom.client.FocusHandler;
import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOutHandler;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.dom.client.MouseOverHandler;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.FocusPanel;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.Widget;

public class HourMinutePicker extends Composite {

	public enum PickerFormat {
		_12_HOUR, _24_HOUR
	};

	private PickerFormat format;
	private int startHour;
	private int endHour;
	private int timeSlices;
	private dbDateTimeBox p_dbDateTimeBox = null;

	/*
	 * User-selected time attributes
	 */
	private int selectedHour = -1;
	private int selectedMinute = -1;
	private String selectedSuffix = "";

	/*
	 * Working copy of the time attributes
	 */
	private int workingHour = -1;
	private int workingMinute = -1;
	private String workingSuffix = "";

	private FlowPanel pnlMain = new FlowPanel();
	private InlineLabel lblInput = new InlineLabel();
	private FocusPanel pnlFocus = new FocusPanel();
	private FlowPanel pnlPopup = new FlowPanel();

	private FlowPanel pnlSuffix = new FlowPanel();
	private FlowPanel pnlHoursAM = new FlowPanel();
	private FlowPanel pnlHoursPM = new FlowPanel();
	private FlowPanel pnlMinutes = new FlowPanel();

	private ArrayList<InlineLabel> hourLabels = new ArrayList<InlineLabel>();
	private ArrayList<InlineLabel> minuteLabels = new ArrayList<InlineLabel>();
	private InlineLabel amSuffixLabel;
	private InlineLabel pmSuffixLabel;
	private InlineLabel clearLabel;
	protected boolean closePopupOnBlur = false;

	private ClickHandler suffixClickHandler = new ClickHandler() {

		@Override
		public void onClick(ClickEvent event) {
			closePopupOnBlur = true;
			Widget source = (Widget) event.getSource();
			source.addStyleName("selected");

			String suffix = source.getElement().getPropertyString("suffix");
			int hour = workingHour;
			int minute = workingMinute == -1 ? 0 : workingMinute;

			if (workingHour == -1) {
				hour = startHour;
			}
			if ("AM".equals(suffix)) {
				pmSuffixLabel.removeStyleName("state-hover");
				pmSuffixLabel.removeStyleName("selected");
				hour = hour % 12 >= startHour ? hour % 12 : startHour;
			} else if ("PM".equals(suffix)) {
				amSuffixLabel.removeStyleName("state-hover");
				amSuffixLabel.removeStyleName("selected");
				hour = (hour % 12) + 12 < endHour ? (hour % 12) + 12 : 12;
			} else {
				hour = -1;
				minute = -1;
			}
			pnlFocus.setFocus(true);
			refreshWidget(suffix, hour, minute);
		}
	};

	private MouseOverHandler suffixSelectedHandler = new MouseOverHandler() {

		@Override
		public void onMouseOver(MouseOverEvent event) {
			closePopupOnBlur = false;
			Widget source = (Widget) event.getSource();
			source.addStyleName("state-hover");
		}
	};

	private ClickHandler hourClickHandler = new ClickHandler() {

		@Override
		public void onClick(ClickEvent event) {
			closePopupOnBlur = true;
			Widget source = (Widget) event.getSource();
			for (InlineLabel inlineLabel : hourLabels) {
				if (inlineLabel.getStyleName().contains("selected")) {
					inlineLabel.removeStyleName("state-hover");
					inlineLabel.removeStyleName("selected");
				}
			}
			source.addStyleName("selected");
			pnlFocus.setFocus(true);
			refreshWidget((workingSuffix == null || "".equals(workingSuffix)) ? "AM" : workingSuffix,
					source.getElement().getPropertyInt("hour"), workingMinute == -1 ? 0 : workingMinute);
		}
	};

	private MouseOverHandler hourSelectedHandler = new MouseOverHandler() {

		@Override
		public void onMouseOver(MouseOverEvent event) {
			closePopupOnBlur = false;
			Widget source = (Widget) event.getSource();
			source.addStyleName("state-hover");
		}
	};

	private ClickHandler minuteClickHandler = new ClickHandler() {

		@Override
		public void onClick(ClickEvent event) {
			closePopupOnBlur = true;
			for (InlineLabel inlineLabel : minuteLabels) {
				if (inlineLabel.getStyleName().contains("selected")) {
					inlineLabel.removeStyleName("selected");
					inlineLabel.removeStyleName("state-hover");
				}
			}
			Widget source = (Widget) event.getSource();
			source.addStyleName("selected");
			pnlFocus.setFocus(true);
			refreshWidget((workingSuffix == null || "".equals(workingSuffix)) ? "AM" : workingSuffix,
					workingHour == -1 ? startHour : workingHour, source.getElement().getPropertyInt("minute"));
		}
	};

	private MouseOverHandler minuteSelectedHandler = new MouseOverHandler() {
		@Override
		public void onMouseOver(MouseOverEvent event) {
			closePopupOnBlur = false;
			Widget source = (Widget) event.getSource();
			source.addStyleName("state-hover");
		}
	};

	private ClickHandler clearPopupHandler = new ClickHandler() {
		@Override
		public void onClick(ClickEvent event) {
			selectedHour = workingHour;
			selectedMinute = workingMinute;
			selectedSuffix = workingSuffix;
			pnlPopup.setVisible(false);
			clear();
			// call the onSelect method from control
			onSelect();
		}
	};

	private MouseOutHandler isOkToCloseHandler = new MouseOutHandler() {

		@Override
		public void onMouseOut(MouseOutEvent event) {
			closePopupOnBlur = true;
			Widget source = (Widget) event.getSource();
			if (!source.getStyleName().contains("selected")) {
				source.removeStyleName("state-hover");
			}
		}
	};

	public HourMinutePicker(dbDateTimeBox dbDateTimeBox, PickerFormat format, int startHour, int endHour, int timeSlices) {
		this.p_dbDateTimeBox = dbDateTimeBox;
		this.startHour = Math.abs(startHour % 25);
		this.endHour = Math.abs(endHour + 1 % 25);
		this.timeSlices = timeSlices;
		this.format = format;
		createBody();
		initWidget(pnlMain);
	}

	public HourMinutePicker(dbDateTimeBox dbDateTimeBox, PickerFormat format) {
		this(dbDateTimeBox, format, 0, 24, 4);
	}

	public HourMinutePicker(PickerFormat format) {
		this(null, format);
	}

	/**
	 * Creates the widget panels
	 */
	private void createBody() {

		pnlMain.addStyleName("timepickr-main");
		pnlMain.add(pnlFocus);

		pnlFocus.setStyleName("timepickr-display");
		pnlFocus.add(lblInput);

		pnlFocus.addFocusHandler(new FocusHandler() {
			@Override
			public void onFocus(FocusEvent event) {
				pnlPopup.setVisible(true);
				refreshWidget(selectedSuffix, selectedHour, selectedMinute);
				closePopupOnBlur = true;
			}
		});

		pnlFocus.addBlurHandler(new BlurHandler() {
			@Override
			public void onBlur(BlurEvent event) {
				if (closePopupOnBlur) {
					refreshWidget(selectedSuffix, selectedHour, selectedMinute);
					pnlPopup.setVisible(false);
				}
			}
		});

		pnlHoursAM.addStyleName("timepickr-row");
		pnlHoursPM.addStyleName("timepickr-row");
		pnlHoursPM.setVisible(false);
		pnlMinutes.addStyleName("timepickr-row");
		pnlSuffix.addStyleName("timepickr-row");

		for (int i = 0; i < timeSlices; i++) {
			int min = i * (60 / timeSlices);
			InlineLabel minuteLabel = createInlineLabel(NumberFormat.getFormat("00").format(min), "timepickr-button");
			minuteLabel.addMouseOverHandler(minuteSelectedHandler);
			minuteLabel.addClickHandler(minuteClickHandler);
			minuteLabel.addMouseOutHandler(isOkToCloseHandler);
			minuteLabel.getElement().setPropertyInt("minute", i);
			pnlMinutes.add(minuteLabel);
			minuteLabels.add(minuteLabel);
		}

		// AM hours
		for (int i = (startHour < 12 ? startHour : 12); i < (endHour < 12 ? endHour : 12); i++) {
			InlineLabel hourLabelAM = createInlineLabel(NumberFormat.getFormat("00").format(i), "timepickr-button");
			hourLabelAM.addMouseOverHandler(hourSelectedHandler);
			hourLabelAM.addClickHandler(hourClickHandler);
			hourLabelAM.addMouseOutHandler(isOkToCloseHandler);
			hourLabelAM.getElement().setPropertyInt("hour", i);
			pnlHoursAM.add(hourLabelAM);
			hourLabels.add(hourLabelAM);
		}

		// PM hours
		for (int i = (startHour < 12 ? 12 : startHour); i < (endHour < 12 ? 12 : endHour); i++) {
			InlineLabel hourLabelPM = createInlineLabel(
					NumberFormat.getFormat("00").format(PickerFormat._24_HOUR.equals(format) ? i : (i > 12 ? i % 12 : i)), "timepickr-button");
			hourLabelPM.addMouseOverHandler(hourSelectedHandler);
			hourLabelPM.addClickHandler(hourClickHandler);
			hourLabelPM.addMouseOutHandler(isOkToCloseHandler);
			hourLabelPM.getElement().setPropertyInt("hour", i);
			pnlHoursPM.add(hourLabelPM);
			hourLabels.add(hourLabelPM);
		}

		if (startHour < 12 && endHour > 12) {
			if (startHour < 12) {
				amSuffixLabel = createInlineLabel("AM", "timepickr-button");
				amSuffixLabel.getElement().setPropertyString("suffix", "AM");
				amSuffixLabel.addMouseOverHandler(suffixSelectedHandler);
				amSuffixLabel.addClickHandler(suffixClickHandler);
				amSuffixLabel.addMouseOutHandler(isOkToCloseHandler);
			} else {
				pnlHoursPM.setVisible(true);
				pnlHoursAM.setVisible(false);
			}

			if (endHour > 12) {
				pmSuffixLabel = createInlineLabel("PM", "timepickr-button");
				pmSuffixLabel.getElement().setPropertyString("suffix", "PM");
				pmSuffixLabel.addMouseOverHandler(suffixSelectedHandler);
				pmSuffixLabel.addClickHandler(suffixClickHandler);
				pmSuffixLabel.addMouseOutHandler(isOkToCloseHandler);
			}

			pnlSuffix.add(amSuffixLabel);
			pnlSuffix.add(pmSuffixLabel);
		}
		clearLabel = createInlineLabel("clear", "timepickr-button");
		clearLabel.getElement().setPropertyString("suffix", "");
		clearLabel.addMouseOverHandler(suffixSelectedHandler);
		clearLabel.addClickHandler(clearPopupHandler);
		clearLabel.addMouseOutHandler(isOkToCloseHandler);

		pnlSuffix.add(clearLabel);
		pnlPopup.add(pnlSuffix);

		pnlPopup.add(pnlHoursAM);
		pnlPopup.add(pnlHoursPM);
		pnlPopup.add(pnlMinutes);

		pnlPopup.addStyleName("timepickr-popup");
		pnlPopup.setVisible(false);

		pnlMain.add(pnlPopup);

	}

	/**
	 * Refreshes the input text
	 */
	private void refreshInputText() {
		String timeText = "";

		if (workingHour > -1) {
			if (format.equals(PickerFormat._12_HOUR)) {
				timeText += NumberFormat.getFormat("00").format(workingHour % 12 == 0 ? 12 : workingHour % 12) + ":";
				timeText += NumberFormat.getFormat("00").format(workingMinute * (60 / timeSlices));
				timeText += workingSuffix;
			} else {
				timeText += NumberFormat.getFormat("00").format(workingHour) + ":";
				timeText += NumberFormat.getFormat("00").format(workingMinute * (60 / timeSlices));
			}
		}

		lblInput.setText(timeText);
	}

	/**
	 * Re-paints the widget using the provided suffix, hour and minute.
	 */

	public void refreshWidget(String suffix, int hour, int minute) {

		if (!this.workingSuffix.equals(suffix)) {
			this.workingSuffix = suffix;
			this.selectedSuffix = this.workingSuffix;
			if ("AM".equals(this.workingSuffix)) {
				pnlHoursAM.setVisible(true);
				pnlHoursPM.setVisible(false);
			} else if ("PM".equals(this.workingSuffix)) {
				pnlHoursAM.setVisible(false);
				pnlHoursPM.setVisible(true);
			} else {
				pnlHoursAM.setVisible(true);
				pnlHoursPM.setVisible(false);
			}
		}


		if (this.workingHour != hour) {
			this.workingHour = hour;
			this.selectedHour = this.workingHour;
		}
		if (this.workingMinute != minute) {
			this.workingMinute = minute;
			this.selectedMinute = this.workingMinute;
		}

		refreshInputText();
	}

	/**
	 * Create an inline label with the list of styles
	 */
	private InlineLabel createInlineLabel(String txt, String... styles) {
		InlineLabel lbl = new InlineLabel(txt);
		for (String style : styles) {
			lbl.addStyleName(style);
		}
		return lbl;
	}

	/**
	 * Get the time in minutes since midnight
	 */
	public Integer getMinutes() {
		if (selectedHour > -1) {
			return selectedHour * 60 + (selectedMinute * (60 / timeSlices));
		} else {
			return null;
		}
	}

	/**
	 * Reset the hour/minute
	 */
	public void clear() {
		this.selectedHour = -1;
		this.selectedMinute = -1;
		this.selectedSuffix = "";
		for (InlineLabel inlineLabel : hourLabels) {
			inlineLabel.removeStyleName("state-hover");
			inlineLabel.removeStyleName("selected");
		}

		for (InlineLabel inlineLabel : minuteLabels) {
			inlineLabel.removeStyleName("state-hover");
			inlineLabel.removeStyleName("selected");
		}
		amSuffixLabel.removeStyleName("state-hover");
		amSuffixLabel.removeStyleName("selected");
		pmSuffixLabel.removeStyleName("state-hover");
		pmSuffixLabel.removeStyleName("selected");
		refreshWidget(selectedSuffix, selectedHour, selectedMinute);
	}

	public void onSelect() {
		// something is changed
		if (this.p_dbDateTimeBox != null)
			p_dbDateTimeBox.valueChange();
	}

}