package gwt_ui.client.controls;

import gwt_ui.client.forms.VForm;

public interface LazyCaller {
	
	VForm onCall();

}
