package gwt_ui.client.controls;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.RichTextArea;
import com.google.gwt.user.client.ui.Widget;

import gwt_sql.shared.DBRecord;
import gwt_sql.shared.DebugUtils;

public class dbRichTextArea extends Composite implements Control {

	public DBRecord R;
	public String colName;
	@UiField(provided = true)
	RichTextArea rta;
	@UiField
	Button btnSave, btnCancel;

	interface MyUiBinder extends UiBinder<Widget, dbRichTextArea> {
	}

	private static MyUiBinder uiBinder = GWT.create(MyUiBinder.class);

	/**
	 * constructor
	 * 
	 * @param strColName
	 *            - name of the field
	 */
	public dbRichTextArea(String strColName) {
		colName = strColName;
		rta = new RichTextArea();

		initWidget(uiBinder.createAndBindUi(this));
	}

	@Override
	public void refresh() {

		if (R != null) {
			this.rta.setHTML(R.getString(this.colName));
		} else
			DebugUtils.W("R is null");

	} // Refresh

	@Override
	public void setR(DBRecord R1) {
		this.R = R1;
	} // SetR

	@Override
	public String getLinkedField() {
		return colName;
	}

	@Override
	public String getType() {
		return this.getClass().getSimpleName();
	}

	@UiHandler("btnSave")
	void btnSave_click(ClickEvent e) {

		if (dbRichTextArea.this.R != null) {
			dbRichTextArea.this.R.put(dbRichTextArea.this.colName, dbRichTextArea.this.rta.getHTML());
		}
	} // Save

	@UiHandler("btnCancel")
	void btnCancel_click(ClickEvent e) {

		refresh();
	} // Cancel
}
