package gwt_ui.client.controls;

import java.util.ArrayList;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.Event.NativePreviewEvent;
import com.google.gwt.user.client.Event.NativePreviewHandler;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Widget;

public class PopButton extends Composite {

	interface MyUiBinder extends UiBinder<Widget, PopButton> {
	}

	private static MyUiBinder uiBinder = GWT.create(MyUiBinder.class);
	/* will keep all the buttons that are displayed */
	private ArrayList<Button> btnList = new ArrayList<Button>();

	@UiField(provided = true)
	Button btnPop;
	@UiField(provided = true)
	FlowPanel fp;

	protected HandlerRegistration registration;

	private final NativePreviewHandler handler = new NativePreviewHandler() {
		@Override
		public void onPreviewNativeEvent(NativePreviewEvent event) {
			if (event.getTypeInt() == Event.ONMOUSEDOWN && fp.isVisible()) {
				// we need this handling because of the event propagation,
				// otherwise the event will stop here and the actual click will
				// not be sent to the real button, it will stop here
				Timer tmr = new Timer() {
					@Override
					public void run() {
						fp.setVisible(!isVisible());
					}
				};
				tmr.schedule(500);
			}
		}
	};

	public PopButton() {
		this("right");
	}

	/**
	 * Will display a popup button list
	 * 
	 * @param direction
	 *            The direction to display the list of buttons. Valid values
	 *            left, right. Default value right.
	 */
	public PopButton(String direction) {
		String leftRight = (direction == "left") ? "menu-open-left" : "menu-open-right";
		btnPop = new Button();
		btnPop.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				if (!fp.isVisible()) {
					registration = Event.addNativePreviewHandler(handler);
					fp.setVisible(true);
				} else {
					if (registration != null) {
						registration.removeHandler();
					}
					fp.setVisible(false);
				}
			}
		});

		fp = new FlowPanel();
		fp.setVisible(false);
		fp.setStyleName("bc-button-menu " + leftRight);

		initWidget(uiBinder.createAndBindUi(this));

	}

	/**
	 * Will add buttons to the displayed popup.
	 * 
	 * @param btn
	 *            The button that is to be added
	 */
	public void addBtnToPopup(Button btn) {
		btnList.add(btn);
		btn.setStyleName("bc-button-link");
		fp.add(btn);
	}

	/**
	 * Will remove the handler when the page is changed
	 */
	@Override
	public void onUnload() {
		if (registration != null) {
			registration.removeHandler();
		}
	}
}