package gwt_ui.client.controls;

import gwt_sql.shared.DBRecord;

/**
 * Interface for all visual controls
 * 
 * @author Fagadar-Ghisa Bogdan
 *
 */
public interface Control {
	/**
	 * Will refresh the value of the control with the value stored in the
	 * DBRecord field that is attached to this control
	 */
	void refresh();

	/**
	 * Will assign a DBRecord to the control
	 * 
	 * @param R
	 *            The DBRecord that will be assigned to control
	 */
	void setR(DBRecord R);

	/**
	 * Will return the database field name that the control is linked to. This
	 * will be the field in the database table that the control will save the
	 * value to.
	 * 
	 * @return Database field name
	 */
	String getLinkedField();

	/**
	 * Will return the type of the control.
	 * 
	 * @return Will return <i>dbTextArea</i> for example
	 * 
	 */
	String getType();

}
