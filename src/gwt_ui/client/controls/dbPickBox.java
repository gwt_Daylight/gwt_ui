package gwt_ui.client.controls;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;

import gwt_sql.client.DBService;
import gwt_sql.client.DBServiceAsync;
import gwt_sql.shared.DBRecord;
import gwt_sql.shared.DebugUtils;
import gwt_ui.client.forms.DialogSelectForm;
import gwt_ui.client.forms.VForm;

public class dbPickBox extends VForm implements Control {

	public DBRecord R;
	public String colName;
	String strTableName;
	String strShowField;
	String strKeyField;
	String strFilterCondition = "";
	String strOrder = "";
	String strSQLCommand = "";
	public String strLinkedField = "";

	VForm form = null;
	LazyCaller lcaller = null;

	public @UiField TextBox textBox;
	public @UiField Button btnGet;

	interface MyUiBinder extends UiBinder<Widget, dbPickBox> {
	}

	private static MyUiBinder uiBinder = GWT.create(MyUiBinder.class);

	private final DBServiceAsync dbService = GWT.create(DBService.class);

	/**
	 * constructor
	 * 
	 * @param f
	 *           - the form that will pe showed to select an item
	 * @param p_strTableName
	 *           - source table for items
	 * @param p_strShowField
	 *           - name of the showed field
	 * @param p_strKeyField
	 *           - name for the ID field
	 * @param p_strFilterCondition
	 *           - optional filter (can pe empty)
	 * @param p_strOrder
	 *           - optional order (can be empty)
	 * @param p_strLinkedField
	 *           - linked field (foreign key)
	 * 
	 * @example new dbPickBox(new FrmCategorie("Category1", "CATEG1", "NAME",
	 *          "CAT1ID"), "CATEG1", "NAME", "CAT1ID", "", "", "CAT1ID");
	 */
	public dbPickBox(VForm f, String p_strTableName, String p_strShowField, String p_strKeyField, String p_strFilterCondition, String p_strOrder,
			String p_strLinkedField) {
		form = f;
		this.strTableName = p_strTableName;
		this.strShowField = p_strShowField;
		this.strKeyField = p_strKeyField;
		this.strFilterCondition = p_strFilterCondition;
		this.strOrder = p_strOrder;
		this.strLinkedField = p_strLinkedField;
		initWidget(uiBinder.createAndBindUi(this));
		textBox.setReadOnly(true);
	} // dbPickBox

	/**
	 * constructor
	 * 
	 * @param l
	 *           - LazyCaller object - the VForm will be created later
	 * @param p_strTableName
	 *           - source table for items
	 * @param p_strShowField
	 *           - name of the showed field
	 * @param p_strKeyField
	 *           - name for the ID field
	 * @param p_strFilterCondition
	 *           - optional filter (can pe empty)
	 * @param p_strOrder
	 *           - optional order (can be empty)
	 * @param p_strLinkedField
	 *           - linked field (foreign key)
	 * 
	 * @example
	 * 
	 * 			new dbPickBox1(new LazyCaller() { public VForm onCall() { return
	 *          new FrmCategorie("Category1", "CATEG", "NAME", "CATID"); } },
	 *          "CATEG", "NAME", "CATID", "", "", "CATID");
	 */
	public dbPickBox(LazyCaller l, String p_strTableName, String p_strShowField, String p_strKeyField, String p_strFilterCondition, String p_strOrder,
			String p_strLinkedField) {
		lcaller = l;
		this.strTableName = p_strTableName;
		this.strShowField = p_strShowField;
		this.strKeyField = p_strKeyField;
		this.strFilterCondition = p_strFilterCondition;
		this.strOrder = p_strOrder;
		this.strLinkedField = p_strLinkedField;
		initWidget(uiBinder.createAndBindUi(this));
		textBox.setReadOnly(true);
	} // dbPickBox

	@Override
	public void setR(DBRecord R1) {
		this.R = R1;
	}

	/**
	 * 
	 * @param b
	 */
	public void setEnabled(boolean b) {
		this.btnGet.setEnabled(b);
	}

	/**
	 * 
	 * GetButton
	 */
	@UiHandler("btnGet")
	void btnGet_onClick(ClickEvent e) {
		if (lcaller == null)
			new DialogSelectForm(form, this, "");
		else
			new DialogSelectForm(lcaller.onCall(), this, "");

	} // @UiHandler("btnGet")

	/**
	 * set the filter
	 * 
	 * @param strFilterText
	 */
	public void setFilter(String strFilterText) {
		this.strFilterCondition = strFilterText;
	}

	@Override
	public void refresh() {

		if (this.strLinkedField.equals(""))
			return;
		String KeyValue = R.getString(this.strLinkedField.toUpperCase());
		if (KeyValue == null)
			return;
		KeyValue = KeyValue.trim();
		try {
			/* search in the record set */
			dbService.GetDBRecord(strTableName, strKeyField, KeyValue, new AsyncCallback<DBRecord>() {

				@Override
				public void onFailure(Throwable caught) {
					DebugUtils.W("dbPickBox.GetDBRecord.Fail!");
				}

				@Override
				public void onSuccess(DBRecord result) {
					if (result.tableName.isEmpty())
						dbPickBox.this.textBox.setText("");
					else
						dbPickBox.this.textBox.setText(result.getString(strShowField.toUpperCase()));
				}
			});
		} catch (Exception e) {
			Window.alert(e.toString());
		}

	} // Refresh()

	/* called after btnGet click */
	public void onReturn(String type, DBRecord R) {
		// Window.alert("dbPickBox");
		if (dbPickBox.this.R != null) {
			dbPickBox.this.R.put(dbPickBox.this.strLinkedField.toUpperCase(), R.getString(strKeyField.toUpperCase()));
			dbPickBox.this.textBox.setText(R.getString(strShowField.toUpperCase()));
		} else
			DebugUtils.W("The value cannot be modified !");

	}

	@Override
	public DBRecord returnSelected() {
		return null;
	}

	@Override
	public String getLinkedField() {
		return strLinkedField;
	}

	@Override
	public String getType() {
		return this.getClass().getSimpleName();
	}

}
