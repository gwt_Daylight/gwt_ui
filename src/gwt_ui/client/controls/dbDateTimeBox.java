package gwt_ui.client.controls;

import java.util.Date;

import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.datepicker.client.DateBox;
import com.google.gwt.user.datepicker.client.DateBox.DefaultFormat;

import gwt_sql.shared.DBRecord;
import gwt_sql.shared.DateUtils;
import gwt_sql.shared.DebugUtils;
import gwt_ui.client.controls.HourMinutePicker.PickerFormat;

@SuppressWarnings("deprecation")
public class dbDateTimeBox extends Composite implements Control {

	// a dbDateTimeBox and 2 buttons
	public _DateBox dateBox = null;
	public HourMinutePicker hmp;
	public PushButton btn1, btn2;

	public DBRecord R;
	public String colName;

	String Format;

	public dbDateTimeBox(String strColName) {
		this(strColName, "yyyy-MM-dd");
	}

	public dbDateTimeBox(String strColName, String strFormat) {

		colName = strColName;
		Format = strFormat;

		dateBox = new _DateBox(Format);
		hmp = new HourMinutePicker(this, PickerFormat._24_HOUR);
		HorizontalPanel panel = new HorizontalPanel();
		panel.add(dateBox);
		panel.add(hmp);
		panel.setSpacing(8);
		initWidget(panel);

		// change for date
		this.dateBox.addValueChangeHandler(new ValueChangeHandler<Date>() {
			public void onValueChange(ValueChangeEvent<Date> event) {
				valueChange();
			}
		});

	}

	@Override
	public void refresh() {

		// refresh the control
		if (R != null) {
			Date d = null;

			try {
				Object o = R.get(this.colName);
				if (o != null) {
					String strDate = o.toString();
					d = DateUtils.String2DateTime(strDate, Format);
					// refresh the date control
					dateBox.setValue(d);
					// refresh the time control
					d = DateUtils.String2DateTime(strDate);
					hmp.refreshWidget("", d.getHours(), d.getMinutes() / 15);
				} else {
					dateBox.setValue(d);
				}
				// set the format again ... don'u know why ...
				this.setFormat(new DateBox.DefaultFormat(DateTimeFormat.getFormat(Format)));

			} catch (Exception e) {
				DebugUtils.W(e.toString());
			}

		} else
			DebugUtils.W("R is null");

	}

	public void setFormat(DefaultFormat dfltFormat) {
		dateBox.setFormat(dfltFormat);
	}

	public Date getValue() {
		Date d = dateBox.getValue();
		d.setMinutes(hmp.getMinutes());
		return d;
	}

	public void setEnabled(boolean b) {
		this.dateBox.setEnabled(b);
	}

	@Override
	public void setR(DBRecord R1) {
		this.R = R1;
	}

	public void valueChange() {
		// in R - we have the formay YYYY-MM-DD HH:mm
		if (R != null) {
			Date d = dateBox.getValue();
			// add minutes
			d = DateUtils.addMinutes(d, hmp.getMinutes());
			// update R
			R.put(colName, DateUtils.Date2String(d, Format + " HH:mm"));
		} else {
			DebugUtils.W("The value cannot be modified !");
		}
	}

	@Override
	public String getLinkedField() {
		return colName;
	}

	@Override
	public String getType() {
		return this.getClass().getSimpleName();
	}
}