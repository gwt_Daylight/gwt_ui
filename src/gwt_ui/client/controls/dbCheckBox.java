package gwt_ui.client.controls;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.CheckBox;

import gwt_sql.shared.DBRecord;
import gwt_sql.shared.DebugUtils;

public class dbCheckBox extends CheckBox implements Control {
	public DBRecord R;
	public String colName;

	/*
	 * Create and attach - onChange
	 */
	public dbCheckBox(String strColName, String strCaption) {
		colName = strColName;
		this.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				if (dbCheckBox.this.R != null)
					dbCheckBox.this.R.put(dbCheckBox.this.colName, dbCheckBox.this.getValue());
				else
					DebugUtils.W("The value cannot be modified !");
			}
		});
		this.setText(strCaption);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see gwtSql.client.controls.Control#refresh()
	 */
	public void refresh() {

		if (R != null) {
			Object o = R.get(this.colName);
			this.setValue(o.toString() == "true" || o.toString() == "1" ? true : false);
		} else
			DebugUtils.G("R is null");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see gwtSql.client.controls.Control#setR(gwtSql.shared.DBRecord)
	 */
	public void setR(DBRecord R1) {
		this.R = R1;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see gwtSql.client.controls.Control#getLinkedField()
	 */
	public String getLinkedField() {
		return colName;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see gwtSql.client.controls.Control#getType()
	 */
	@Override
	public String getType() {
		return this.getClass().getSimpleName();
	}
}
