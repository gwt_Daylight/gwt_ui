package gwt_ui.client.controls;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.TextBox;

import gwt_sql.client.DBService;
import gwt_sql.client.DBServiceAsync;
import gwt_sql.shared.DBRecord;
import gwt_sql.shared.DebugUtils;

public class dbTextBox extends TextBox implements Control {

	public DBRecord R;
	public String colName;

	// database connector
	private final DBServiceAsync dbService = GWT.create(DBService.class);

	/**
	 * constructor
	 * 
	 * @param strColName
	 *            - name of the field
	 */
	public dbTextBox(String strColName) {
		this(strColName, false);
	}

	/**
	 * constructor
	 * 
	 * @param strColName
	 *            - name of the column
	 * @param bSave
	 *            - autosave
	 */
	public dbTextBox(String strColName, final Boolean bSave) {
		colName = strColName;

		this.addChangeHandler(new ChangeHandler() {
			@Override
			public void onChange(ChangeEvent event) {
				//
				if (dbTextBox.this.R != null) {
					dbTextBox.this.R.put(dbTextBox.this.colName, dbTextBox.this.getText());

					// if save - save the R

					if (bSave) {
						dbTextBox.this.R.isNew = false;
						// save
						dbService.saveDBRecord(R, new AsyncCallback<String>() {
							@Override
							public void onSuccess(String result) {
								// nothing

							}

							@Override
							public void onFailure(Throwable caught) {
								DebugUtils.W("dbTextBox.onChange.saveDBRecord fail");
							}
						});
					}
				} else
					DebugUtils.W("The value cannot be modified !");
			}

		});

	}

	@Override
	public void refresh() {

		if (R != null) {
			Object o = R.get(this.colName);
			this.setText(o.toString());
		} else
			DebugUtils.W("R is null");

	}

	@Override
	public void setR(DBRecord R1) {
		this.R = R1;
	}

	@Override
	public String getLinkedField() {
		return colName;
	}

	@Override
	public String getType() {
		return this.getClass().getSimpleName();
	}

}
